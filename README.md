# THIS PIECE OF SOFTWARE IS DEPRECATED

The shit was made in 2016, is cringy as hell, and probably doesn't work. For fuck's sake, don't try to use it, even under threat of torture or death

---

# BFJS
Yet another Brainfuck compiler.

## Features
It's works.

## Bindings
| Binding   | Unicode support                      |
| --------- | ------------------------------------ |
| NodeJS    | Yes (1 character - Number (8 Bytes)) |
| C         | No (1 character - char (1 Byte))     |
| Java      | Yes (1 character - char (2 Bytes))   |
| Brainfuck | Yes                                  |
| Pascal    | No (1 character - char (1 Byte))     |

## --help
Code:
```
Comments - /*...*/
Commands:
	> - Select next cell
	< - Select previous cell
	+ - Increment cell
	- - Decrement cell
	. - Output cell to stdout
	, - Stdin symbol into cell
	[ - Start loop
	] - End loop
```
Arguments:
```
--help - show this menu
--in [file] - set input file
--out [file] - set output file
--binding [language] - select binding (See "Bindings")
```
