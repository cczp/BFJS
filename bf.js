/*
 * BFJS main module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

var app = {
	//Secondary variables
		binding: "NodeJS",
		
		inFile: "",
		outFile: "stdout",
	
	//Run method
		run: function(argv) {
			var
				inCode,
				outCode;
			
			//Read arguments
				for (var i = 0; i < argv.length; i++) {
					switch (argv[i]) {
						case "--help":
							console.log(
								[
									"BFJS " + require("./package.json").version + " help:",	
										"\tCode:",
											"\t\tComments - /*...*/",
											"\t\tCommands:",
												"\t\t\t> - Select next cell",
												"\t\t\t< - Select previous cell",
												"\t\t\t+ - Increment cell",
												"\t\t\t- - Decrement cell",
												"\t\t\t. - Output cell to stdout",
												"\t\t\t, - Stdin symbol into cell",
												"\t\t\t[ - Start loop",
												"\t\t\t] - End loop",
										"\tArguments:",
											"\t\t--help - show this menu",
											"\t\t--in [file] - set input file",
											"\t\t--out [file] - set output file",
											"\t\t--binding [language] - select binding (" +
												Object.keys(this.outGen.opCodes).join(", ") +
											")"
								].join("\n")
							);
							
							process.exit(1);
						case "--in":
							this.inFile = argv[i + 1];
							
							i++;
							break;
						case "--out":
							this.outFile = argv[i + 1];
							
							i++;
							break;
						case "--binding":
							this.binding = argv[i + 1];
							
							if(!(this.binding in this.outGen.opCodes)) {
								throw new ReferenceError(
									"mode \"" +
									argv[argv.lastIndexOf("--binding") + 1] +
									"\" doesn\'t exist"
								);
								process.exit(1);
							}
							
							i++;
							break;
						default:
							throw new ReferenceError(
								"\"" +
								argv[i] +
								"\" is illegal argument"
							);
							
							break;
					}
				}
				
				if (!(this.inFile)) {
					throw new ReferenceError(
						"no input file"
					);
					process.exit(1);
				}
			
			//Read file
				try {
					inCode = require("fs").readFileSync(this.inFile, "utf-8");
				} catch(e) {
					throw new ReferenceError(
						"file \"" +
						this.inFile +
						"\" doesn\'t exist"
					);
					process.exit(1);
				}
			
			//Compile
				inCode = this.preprocessor.run(inCode);
				outCode = this.outGen.run(inCode);
			
			//Write to file
				if (this.outFile == "stdout") {
					console.log(outCode);
				} else {
					try {
						require("fs").writeFileSync(this.outFile, outCode);
					} catch(e) {
						throw e;
					}
				}
		},
	
	//Objects
		preprocessor: {
			//Secondary variables
				toDelete: [
					/(\/\*(?:(?!\*\/).|[\n\r])*\*\/)/mg, //Comment
					/	/mg, //Tab
					/ /mg, //Space
					/(\r\n|\n|\r)/mg, //EOL
				],
			
			run: function(code) {
				for (var i = 0; i < this.toDelete.length; i++) {
					code = code.replace(this.toDelete[i], "");
				}
				
				return code;
			}
		},
		
		outGen: {
			//Secondary variables
				opNames: {
					">": "nextCell",
					"<": "prevCell",
					"+": "incCell",
					"-": "decCell",
					".": "outCell",
					",": "inToCell",
					"[": "loop",
					"]": "unloop"
				},
				
				opCodes: {
					NodeJS: {
						_start:
							"require(\"readline\").createInterface(process.stdin, process.stdout).question(\"Input: \", function(e){\n" +
							"var mem = [], str = e, pnt, strPnt = 0;\n" +
							"for (pnt = 0; pnt < 30000; pnt++) {mem[pnt] = 0}\n" +
							"pnt = 0;\n",
						nextCell: "pnt++;\n",
						prevCell: "pnt--;\n",
						incCell: "mem[pnt]++;\n",
						decCell: "mem[pnt]--;\n",
						outCell: "process.stdout.write(String.fromCharCode(mem[pnt]));\n",
						inToCell: "mem[pnt] = str.charCodeAt(strPnt); strPnt++;\n",
						loop: "while (mem[pnt]) {\n",
						unloop: "}\n",
						_end: 
							"process.exit(1);\n" + 
							"});"
					},
					
					C: {
						_start:
							"#include <stdio.h>\n" +
							"void main() {\n" +
							"unsigned char mem[30000];\n" +
							"unsigned char str[256];\n" +
							"printf(\"Input: \");\n" +
							"fgets(str, 255, stdin);\n" + 
							"unsigned char *pnt = mem, *str_pnt = str;\n",
						nextCell: "++pnt;\n",
						prevCell: "--pnt;\n",
						incCell: "++*pnt;\n",
						decCell: "--*pnt;\n",
						outCell: "printf(\"%c\", *pnt);\n",
						inToCell: "*pnt = *str_pnt; ++str_pnt;\n",
						loop: "while (*pnt) {\n",
						unloop: "}\n",
						_end: "}"
					},
					
					Java: {
						_start:
							"import java.util.Scanner;\n" + 
							"class Program {\n" +
							"public static void main(String argv[]) {\n" + 
							"char mem[] = new char[30000];\n" +
							"System.out.print(\"Input: \");\n" +
							"String str = new Scanner(System.in).nextLine() + \"\\0\";\n" +
							"short pnt = 0, strPnt = 0;\n",
						nextCell: "pnt++;\n",
						prevCell: "pnt--;\n",
						incCell: "mem[pnt]++;\n",
						decCell: "mem[pnt]--;\n",
						outCell: "System.out.printf(\"%c\", mem[pnt]);\n",
						inToCell: "mem[pnt] = str.charAt(strPnt); strPnt++;\n",
						loop: "while (mem[pnt] != 0) {\n",
						unloop: "}\n",
						_end: "}}"
					},
					
					BrainFuck: {
						_start: "",
						nextCell: ">",
						prevCell: "<",
						incCell: "+",
						decCell: "-",
						outCell: ".",
						inToCell: ",",
						loop: "[",
						unloop: "]",
						_end: ""
					},
					
					Pascal: {
						_start:
							"var\n" +
							"mem : array [1..29999] of char;\n" +
							"str : string;\n" +
							"pnt, strPnt : integer;\n" +
							"begin\n" +
							"pnt := 1; strPnt := 1;\n" +
							"write(\'Input: \');\n" +
							"readln(str);\n",
						nextCell: "pnt := pnt + 1;\n",
						prevCell: "pnt := pnt -1;\n",
						incCell: "mem[pnt] := char(integer(mem[pnt]) + 1);\n",
						decCell: "mem[pnt] := char(integer(mem[pnt]) - 1);\n",
						outCell: "write(mem[pnt]);\n",
						inToCell: "mem[pnt] := str[strPnt]; strPnt := strPnt + 1;\n",
						loop:
							"while mem[pnt] <> #0 do\n" +
							 "begin\n",
						unloop: "end;\n",
						_end: "end."
					}
				},
			
			run: function(code) {
				var readyCode = "";
				
				readyCode += this.opCodes[app.binding]._start;
				for (var i = 0; i < code.length; i++) {
					if (code[i] in this.opNames) {
						readyCode += this.opCodes[app.binding][this.opNames[code[i]]];
					} else {
						throw new SyntaxError(
							"command \"" +
							code[i] +
							"\" doesn\'t exist",
							
							app.inFile
						);
						process.exit(1);
					}
				}
				readyCode += this.opCodes[app.binding]._end;
				
				return readyCode;
			}
		}
};
	
app.run(process.argv.slice(2));
